<?php

/*
  This is a collection of functions that abstract out various WP API calls
  General goal is to reduce repetiton and make wordpress a little easier to use.
*/

/*
 * This function simply creates "the loop" and prints out the page title and content.
 * The title will be echoed with an h1 tag with the id attribute set to the page slug.
*/ 
function the_loop_for_title_and_content()
{
  if(have_posts()):
    while(have_posts()): the_post();
      echo '<h1 id="'.the_slug().'-title">'.get_the_title().'</h1>', PHP_EOL;
      the_content();
    endwhile;
  endif;
}

/*
 * This function will create "the loop" but only print the title h1 tag.
 * The content of the page will be stored in the variable passed into the function.
 * You can then use that variable to echo the content on a different part of the template.
 * 
*/
function the_loop_for_title_giving_content(&$page_content)
{
  if(have_posts()):
    while(have_posts()): the_post();
      echo '<h1 id="'.the_slug().'-title">'.get_the_title().'</h1>', PHP_EOL;
      $page_content = get_the_content();
    endwhile;
  endif;
}

/*
 * This function will create "the loop" but only print out the title h1 tag.
*/
function the_loop_for_title()
{
  if(have_posts()):
    while(have_posts()): the_post();
      echo '<h1 id="'.the_slug().'-title">'.get_the_title().'</h1>', PHP_EOL;
    endwhile;
  endif;
}

/*
 * This function will create "the loop" but only print out the content.
 * You can pass an integer as the first parameter to truncate the content to that length.
*/
function the_loop_for_content($length_limit = FALSE)
{
  if(have_posts()):
    while(have_posts()): the_post();
      if(!$length_limit):
        the_content();
      else:
        echo truncate(get_the_content(), $length_limit);
      endif;
    endwhile;
  endif;
}

/*
 * Returns the slug of the current page.
*/
function the_slug()
{
  $post_data = get_post($post->ID, ARRAY_A);
  return $post_data['post_name'];
}

/*
 * Returns an array of custom fields with the given name.
 * If the single flag is set, only the first item of the custom fields with the given name is returned.
 * If the post does not have any custom fields with the name given, return an empty array.
*/
function the_custom_fields($post_id = NULL, $custom_field_name, $single = FALSE)
{
  if($post_id === NULL || $post_id === FALSE) $post_id = get_the_ID();
  $custom_fields = get_post_custom($post_id);

  if(isset($custom_fields[$custom_field_name]))
    return (!$single ? $custom_fields[$custom_field_name] : array_pop($custom_fields[$custom_field_name]));

  return (!$single) ? array() : "";
}

// Alias for the above
function get_custom_fields($post_id = NULL, $custom_field_name, $single = FALSE)
{
  return the_custom_fields($post_id, $custom_field_name, $single);
}

/*
 * Returns the first custom field with the given name. Only gives a single value.
 * If the post does not have a custom field with the name given, return FALSE.
 * Just calls the above method internally with the single flag set to TRUE.
*/
function the_custom_field($post_id = NULL, $custom_field_name)
{
  return the_custom_fields($post_id, $custom_field_name, TRUE);
}

// Alias for the above
function get_custom_field($custom_field_name)
{
  return the_custom_field($custom_field_name);
}

/* 
 * Returns an array of the taxonomies the post belongs to.
 * If the single flag is set, only the first taxonomy the post belongs to is given
*/
function taxonomies($post_type, $single = FALSE)
{
  $terms = get_the_terms(get_the_ID(), $post_type);

  if($terms->errors) {
    print_r($terms->errors);
    die;
  }

  if($terms)
    return (!$single ? $terms : array_pop($terms));

  return FALSE;
}

/* 
 * Returns the first of the taxonomies the post belongs to.
 * Just calls the above method internally with the single flag set to TRUE.
*/
function the_taxonomy($post_type)
{
  return taxonomies($post_type, TRUE);
}

/*
 * Returns the name of a term with the given slug for the given taxonomy
*/ 
function taxonomy_name($term_slug, $taxonomy)
{
  $term = get_term_by('slug', $term_slug, $taxonomy);
  if($term) return $term->name;
  return FALSE;
}

/*
 * This function can be used to print out content, links, etc. for all of the posts under a given taxonomy for a given type of post.
 * For example. You can print out links for all of the posts of type "staff" under the taxonomy "executive-team".
 * Or, you could print the post thumb, title with permalink, and content for the posts of type "experiences" under the taxonomy "games".
 *
 * $post_type is the type of post you want to loop over, i.e. a custom post type of "partners".
 * $taxonomy is the name of the taxonomy of the posts, i.e. "production".
 * You can pass in FALSE for the taxonomy to just pull all posts of the given post type, ignoring taxonomy.
 * 
 * By default the loop will simply echo out the post's permalink with the post's title as the anchor text.
 * However, you can specify what else you would like to print out using the $html_callback. More on that below.
 *
 * $args allows you to supply ordering and other query arguments for the query.
 * These will be given directly to WP_Query to pull the posts for the list of links.
 * The query arguments "post_type" and {post_type}_type are given automatically and some sensible defaults assumed.
 * {post_type}_type should be the name of your taxonomy for the given post type.
 * For example. If you have a post type of "partners" then your taxonomy should be named partner_types.
 *
 * $html_callback allows you to pass the name of a function to use for the post formatting, in case the default isn't suitable.
 * This function should return the HTML that will be used for the post and can include WP theme functions.
 * This function will be called once per post, i.e. once for each post that is returned by the query.
 *
 * $callback_args allows you to pass arguments to your html formatting callback. This should be an associative array of variables.
 * 
 * Important:   Currently, all custom posts using this function must have an "order" custom field (created using the custom post type plugin)
 *              This is used to sort the custom post types by that value. In the future we'll probably want to tweak this so by default it isn't required.
*/ 
function content_for_post_type_of_taxonomy($post_type, $taxonomy = FALSE, $args = array(), $html_callback = FALSE, $callback_args = array())
{
  $default_args = array(
    // 'orderby'  => 'meta_value_num',
    // 'meta_key' => 'ecpt_order',
    'order'    => 'ASC',
    'orderby'  => 'title',
    'before'   => '<li>',
    'after'    => '</li>'
  );

  $post_type_taxonomy_args = array('post_type' => $post_type);
  if($taxonomy !== FALSE) $post_type_taxonomy_args[preg_replace('/s$/i', '', $post_type).'_type'] = $taxonomy;

  $query_args = array_merge($post_type_taxonomy_args, $default_args, $args);
  $query = new WP_Query($query_args);

  if($query->have_posts()):
    while($query->have_posts()): $query->the_post();
      if($html_callback === FALSE) {
        echo $args['before'].'<a href="'.get_permalink().'" title="'.get_the_title().'">'.get_the_title().'</a>'.$args['after'], PHP_EOL;
      } else {
        echo $html_callback($callback_args);
      }
    endwhile;
  endif;
}

/*
 * Convenience method for pulling all posts of a custom post type, without specifying a taxonomy.
 * This method simply calls the above method with $taxonomy set to FALSE
*/
function content_for_post_type($post_type, $args = array(), $html_callback, $callback_args = array())
{
  return content_for_post_type_of_taxonomy($post_type, FALSE, $args, $html_callback, $callback_args);
}

/*
 * This function will return an array of photos that exist in the gallery for the current post.
 * It automatically removes the featured post image because it is assumed that photo
 * will be used on the page seperate from the gallery photos, as it is intended.
*/
function get_gallery_photos_excluding_featured()
{
  $featured_photo_id = get_post_thumbnail_id();

  $photos = get_children(array(
    'post_parent' => get_the_ID(),
    'post_status' => 'inherit',
    'post_type' => 'attachment',
    'post_mime_type' => 'image',
    'orderby' => 'menu_order',
    'order' => 'ASC'
  ));

  unset($photos[$featured_photo_id]);
  return $photos;
}

/* 
 * This function will automatically grab and print links for each gallery photo for the current page.
 * The parameter size is used for the image that it outputted and should be a valid size registered with add_image_size
 * The image will be linked to the original size of the photo. Useful for triggering fancybox or other lightboxes.
 * Passing false to the link parameter will disable links on the images.
*/
function loop_for_gallery_photos_of_size($size, $link = TRUE)
{
  $photos = get_gallery_photos_excluding_featured();
  
  if(!$photos) {
    echo '<p><em>No Photos</em></p>';
    } else {
      foreach($photos as $photo) {
        $sized_src    = wp_get_attachment_image_src($photo->ID, $size);
        $original_src = wp_get_attachment_image_src($photo->ID, 'wp_original_image_size');

        if($link) echo '<a href="'.$original_src[0].'" target="_blank">';
        echo '<img src="'.$sized_src[0].'" />';
        if($link) echo '</a>';
        echo PHP_EOL;
      }
    }
}

/*
 * Returns the latest post of the given category or FALSE if none exists.
*/

function get_latest_post($category_id) {
  $latest = get_posts(array(
    'category' => $category_id,
    'numberposts' => 1
  ));

  return (!empty($latest)) ? $latest[0] : FALSE;
}

/*
 * This function will return the root page object of the page id given
*/
function get_root_page($id, $id_only = FALSE)
{
  $current = get_post($id);
  if(!$current->post_parent) {
    return (!$id_only) ? $current : $current->ID;
  } else {
    return get_root_page($current->post_parent, $id_only);
  }
}

/*
 * This function will return the root page ID of the page id given
*/
function get_root_page_id($id)
{
  return get_root_page($id, TRUE);
}

/* 
 * This function will truncate the string given to the size given and append the string $dots at the end.
*/
function truncate($string, $length = 50, $dots = '&hellip;')
{
  if(empty($string) || strlen($string) <= $length) return $string;
  return substr($string, 0, $length).$dots;
}

/*
 * This function will return an email link that is encoded to make email link harvesting slightly harder for bots
*/

function email_link($email, $link_text = NULL, $html_attributes = array())
{
  $email  = str_replace('#', '', $email);
  $length = strlen($email);

  for($i = 0; $i<$length; $i++)
    $obfuscated_email .= "&#" . ord($email[$i]);  // creates ASCII HTML entity

  if($link_text === NULL)
    $link_text = $obfuscated_email;

  if(!empty($html_attributes))
    foreach($html_attributes as $attr => $value)
      $link_attributes .= " {$attr}=\"{$value}\"";

  return '<a href="mailto:' . $obfuscated_email . '"'.$link_attributes.'>'.$link_text.'</a>';
}

/*
 * This function will take a youtube URL and return only the Youtube ID.
*/
function youtube_id($url)
{
  parse_str(parse_url($url, PHP_URL_QUERY), $query_vars);

  if(array_key_exists('v', $query_vars)) {
    return $query_vars['v'];
  }

  return FALSE;
}